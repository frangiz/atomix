﻿using Atomix.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class InputHandler : IInputHandler
	{
		public event Action Quit;

		public ICommand GetNextCommand(Board board)
		{
			var key = Console.ReadKey(true);
			if (key.Key == ConsoleKey.Q && Quit != null) { Quit(); }

			ICommand cmd;
			switch (key.Key)
			{
				case ConsoleKey.UpArrow:
					cmd = new MoveUpCommand(board);
					break;
				case ConsoleKey.DownArrow:
					cmd = new MoveDownCommand(board);
					break;
				case ConsoleKey.LeftArrow:
					cmd = new MoveLeftCommand(board);
					break;
				case ConsoleKey.RightArrow:
					cmd = new MoveRightCommand(board);
					break;
				case ConsoleKey.Spacebar:
					cmd = new SwapCommand(board);
					break;
				default:
					cmd = new NopCommand();
					break;
			}

			return cmd;
		}

		public void OnPlayerDead(PlayerDeadEventArgs args)
		{
			args.Handled = false;
		}

	}
}
