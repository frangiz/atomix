﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix.Commands
{
	public abstract class MoveCommand : ICommand
	{
		public enum Direction { Up, Down, Left, Right }
		private Board _board;
		private int dx = 0;
		private int dy = 0;
		private MapEntity _currentPlayerPosition;
		private bool _abort = false;

		public MoveCommand(Board board, Direction direction)
		{
			_board = board;
			if (direction == Direction.Up) { dy = -1; }
			else if (direction == Direction.Down) { dy = 1; }
			else if (direction == Direction.Left) { dx = -1; }
			else if (direction == Direction.Right) { dx = 1; }
			_currentPlayerPosition = _board.Player.DeepClone();
		}

		public void Execute()
		{
			while (_board.IsEmptySpace(_board.Player.X + dx, _board.Player.Y + dy) &&
							!_board.PlayerReachedGoal() &&
							!_abort)
			{
				_board.Player.X += dx;
				_board.Player.Y += dy;
			}
		}

		public bool CanExecute()
		{
			return _board.IsEmptySpace(_board.Player.X + dx, _board.Player.Y + dy);
		}

		public void Undo()
		{
			_board.Player.SwapPosition(_currentPlayerPosition);
		}

		public void Abort() { _abort = true; }

		public Board GetBoard() { return _board; }
	}

	public class MoveUpCommand : MoveCommand
	{
		public MoveUpCommand(Board board) : base(board, Direction.Up) { }
	}

	public class MoveDownCommand : MoveCommand
	{
		public MoveDownCommand(Board board) : base(board, Direction.Down) { }
	}

	public class MoveLeftCommand : MoveCommand
	{
		public MoveLeftCommand(Board board) : base(board, Direction.Left) { }
	}

	public class MoveRightCommand : MoveCommand
	{
		public MoveRightCommand(Board board) : base(board, Direction.Right) { }
	}
}
