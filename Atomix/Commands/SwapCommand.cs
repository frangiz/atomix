﻿namespace Atomix.Commands
{
	public class SwapCommand : ICommand
	{
		private Board _board;
		public SwapCommand(Board board) { _board = board; }

		public void Execute()
		{
			_board.Player.SwapPosition(_board.Token);
		}

		public bool CanExecute() { return true; }
		public void Undo() { Execute(); }
		public void Abort() { }
		public Board GetBoard() { return _board; }
	}
}
