﻿namespace Atomix.Commands
{
	public class NopCommand : ICommand
	{
		public NopCommand() { }
		public void Execute() { }
		public bool CanExecute() { return true; }
		public void Undo() { }
		public void Abort() { }
		public Board GetBoard() { return null; }
	}
}
