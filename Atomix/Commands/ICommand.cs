﻿namespace Atomix
{
	public interface ICommand
	{
		void Execute();
		bool CanExecute();
		void Undo();
		void Abort();
		Board GetBoard();
	}
}
