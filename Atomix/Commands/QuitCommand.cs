﻿namespace Atomix.Commands
{
	public class QuitCommand : ICommand
	{
		public QuitCommand() { }
		public void Execute() { }
		public bool CanExecute() { return true; }
		public void Undo() { }
		public void Abort() { }
		public Board GetBoard() { return null; }
	}
}
