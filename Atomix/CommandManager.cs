﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class CommandManager
	{
		private Stack<ICommand> _executedCommands;
		public event Action CommandExecuted;

		public CommandManager()
		{
			_executedCommands = new Stack<ICommand>();
		}

		public void ExecuteCommand(ICommand cmd)
		{
			if (cmd.CanExecute())
			{
				_executedCommands.Push(cmd);
				cmd.Execute();
				if (CommandExecuted != null) { CommandExecuted(); }
			}
		}

		public bool IsUndoAvailable() { return _executedCommands.Count > 0; }

		public void UndoLastCommand()
		{
			if (IsUndoAvailable())
			{
				var cmd = _executedCommands.Pop();
				cmd.Abort();
				cmd.Undo();
			}
		}
	}
}
