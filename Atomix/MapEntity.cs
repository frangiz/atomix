﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class MapEntity : INotifyPropertyChanged, IDeepCloneable<MapEntity>
	{
		public string Name { get; set; }

		private int _x;
		public int X
		{
			get { return _x; }
			set
			{
				if (value != _x)
				{
					_x = value;
					OnPropertyChanged("X");
				}
			}
		}

		private int _y;
		public int Y
		{
			get { return _y; }
			set
			{
				if (value != _y)
				{
					_y = value;
					OnPropertyChanged("Y");
				}
			}
		}

		public MapEntity(string name) { Name = name; }

		public bool IsSamePosition(MapEntity me) { return _x == me._x && _y == me._y; }

		public void SwapPosition(MapEntity me)
		{
			var posX = X;
			var posY = Y;
			_x = me.X;
			_y = me.Y;
			me._x = posX;
			me._y = posY;
			OnPropertyChanged("X");
			OnPropertyChanged("Y");
			me.OnPropertyChanged("X");
			me.OnPropertyChanged("Y");
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, e);
		}

		protected void OnPropertyChanged(string propertyName)
		{
			OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region IDeepCloneable<MapEntity>

		public MapEntity DeepClone()
		{
			return new MapEntity(Name)
			{
				_x = X,
				_y = Y
			};
		}

		object IDeepCloneable.DeepClone()
		{
			return DeepClone();
		}

		#endregion
	}
}
