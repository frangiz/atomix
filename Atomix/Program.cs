﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	class Program
	{
		static void Main(string[] args)
		{
			var log = new ConsoleLog();
			var cmdManager = new CommandManager();
			var mode = Console.ReadKey();
			var handler = mode.Key != ConsoleKey.A ? new InputHandler() : (IInputHandler)new AIPlayer(log);
			Console.Clear();
			new Game(MapLoader.LoadMap(), handler, cmdManager, log).Start();
		}
	}
}
