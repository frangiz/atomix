﻿using Atomix.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix.AI
{
	public class PathNode
	{
		public enum GameState { Alive, Dead, FoundTheGoal };
		private List<PathNode> _children;
		public List<PathNode> Children
		{
			private set { }
			get { return _children; }
		}
		private Board _board;
		private ICommand _command;
		private GameState _gameState;
		public GameState CurrentGameState
		{
			private set { }
			get { return _gameState; }
		}
		private EventManager _eventManager;

		public PathNode(Board board)
		{
			_board = board;
			Init();
			
		}

		public PathNode(ICommand command)
		{
			_command = command;
			_board = command.GetBoard();
			Init();
		}

		private void Init()
		{
			_children = new List<PathNode>();
			_gameState = GameState.Alive;

			_eventManager = new EventManager(_board);
			_eventManager.PlayerDead += OnPlayerDead;
			_eventManager.PlayerFinished += OnPlayerFinished;
		}

		public void MakeMove()
		{
			if (_command != null)
			{
				_command.Execute();
			}
		}

		public void BreedChildren()
		{
			if (_gameState != GameState.Alive) { return; }

			var moveUp = new MoveUpCommand(_board.DeepClone());
			if (moveUp.CanExecute()) { _children.Add(new PathNode(moveUp)); }

			var moveDown = new MoveDownCommand(_board.DeepClone());
			if (moveDown.CanExecute()) { _children.Add(new PathNode(moveDown)); }

			var moveLeft = new MoveLeftCommand(_board.DeepClone());
			if (moveLeft.CanExecute()) { _children.Add(new PathNode(moveLeft)); }

			var moveRight = new MoveRightCommand(_board.DeepClone());
			if (moveRight.CanExecute()) { _children.Add(new PathNode(moveRight)); }

			// TODO: Add swap.
		}

		public bool HasGoalState()
		{
			var hasGoalState = false;
			if (_gameState == GameState.FoundTheGoal) { return true; }
			foreach (var child in _children)
			{
				hasGoalState |= child.HasGoalState();
			}

			return hasGoalState;
		}

		public void RemoveNonGoalLeaves()
		{
			_children.ForEach(c => c.RemoveNonGoalLeaves());
			_children.RemoveAll(c => !c.HasGoalState());
		}

		private void OnPlayerDead(PlayerDeadEventArgs args)
		{
			_gameState = GameState.Dead;
			_command.Abort();
		}

		private void OnPlayerFinished()
		{
			_gameState = GameState.FoundTheGoal;
			_command.Abort();
		}
	}
}
