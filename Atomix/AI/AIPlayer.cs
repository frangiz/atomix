﻿using Atomix.AI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Atomix
{
	public class AIPlayer : IInputHandler
	{
		private ILog _log;
		private PathNode _rootNode;
		public PathNode RootNode
		{
			private set { }
			get { return _rootNode; }
		}

		public event Action Quit;

		public AIPlayer(ILog log)
		{
			_log = log;
		}

		public ICommand GetNextCommand(Board board)
		{
			if (_rootNode == null) { FindPath(board); }

			return null;
		}

		public void OnPlayerDead(PlayerDeadEventArgs args)
		{
			_log.Log("Player dead :(");
		}

		// Need to use copies of the EventManager to figure out the player state.
		private void FindPath(Board board)
		{
			_rootNode = new PathNode(board);
			_rootNode.MakeMove();
			_rootNode.BreedChildren();

			var children = new List<PathNode>();
			children.AddRange(_rootNode.Children);
			for (int i = 0; i < 2; i++)
			{
				var newChildren = new List<PathNode>();
				foreach (var child in children)
				{
					child.MakeMove();
					child.BreedChildren();
					newChildren.AddRange(child.Children);
				}
				children.Clear();
				children.AddRange(newChildren);
			}
		}

		// Cut all the branches!
		public void RemoveDeadPaths()
		{
			if (_rootNode.HasGoalState())
			{
				_rootNode.RemoveNonGoalLeaves();
			}
		}
	}
}
