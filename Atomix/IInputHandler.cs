﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public interface IInputHandler
	{
		event Action Quit;

		ICommand GetNextCommand(Board board);
		void OnPlayerDead(PlayerDeadEventArgs args);
	}
}
