﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class EventManager
	{
		public event Action PlayerFinished;
		public event Action<PlayerDeadEventArgs> PlayerDead;

		public EventManager(Board board)
		{
			board.Player.PropertyChanged += (sender, e) => { OnPlayerProperyChange(sender, e, board); };
		}

		private void OnPlayerProperyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e, Board board)
		{
			if (board.PlayerReachedGoal() && PlayerFinished != null)
			{
				PlayerFinished();
			}
			else if (board.Player.X < 0 || board.Player.X >= board.MapWidth ||
								board.Player.Y < 0 || board.Player.Y >= board.MapHeight)
			{
				if (PlayerDead != null)
				{
					PlayerDead(new PlayerDeadEventArgs());
				}
			}
		}
	}

	public class PlayerDeadEventArgs : EventArgs
	{
		public bool Handled = false;
	}
}
