﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class MapLoader
	{
		public static Board LoadMap()
		{
			var map = new int[,]
			{
				{ 0, 0, 0, 0, 1, 0, 0, 0 }, 
				{ 1, 0, 0, 0, 0, 0, 0, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 0, 0, 0, 1, 0, 0 }
			};
			var player = new MapEntity("kalle") { X = 1, Y = 4 };
			var token = new MapEntity("token") { X = 0, Y = 0 };
			var goal = new MapEntity("goal") { X = 7, Y = 2 };
			return new Board(map, player, token, goal);
		}
	}
}
