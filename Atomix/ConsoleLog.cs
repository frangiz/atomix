﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class ConsoleLog : ILog
	{
		private LinkedList<string> _messages;

		public ConsoleLog()
		{
			_messages = new LinkedList<string>();
		}

		public void Log(string msg)
		{
			_messages.AddFirst(msg);
			if (_messages.Count > 10) { _messages.RemoveLast(); }
		}

		public void WriteLines()
		{
			for (int i = 0; i < 5; i++) { Console.WriteLine(""); }
			Console.WriteLine("Log:");
			foreach (var line in _messages)
			{
				Console.WriteLine(line);
			}
		}
	}
}
