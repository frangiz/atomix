﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class Board : IDeepCloneable<Board>
	{
		public MapEntity Player { get; set; }
		public MapEntity Token { get; set; }
		public MapEntity Goal { get; set; }

		private int[,] _map;
		public int MapWidth { private set {} get { return _map.GetLength(1); } }
		public int MapHeight { private set {} get { return _map.GetLength(0); } }
		public int MaxMoves { private set { } get { return 20; } }

		public Board(int[,] map, MapEntity player, MapEntity token, MapEntity goal)
		{
			_map = map;
			Player = player;
			Token = token;
			Goal = goal;
		}

		public void DrawMap()
		{
			for (int i = 0; i < MapHeight; i++)
			{
				for (int j = 0; j < MapWidth; j++)
				{
					if (Player.X == j && Player.Y == i)
					{
						Console.Write("P");
					}
					else if (_map[i, j] == 1)
					{
						Console.Write("X");
					}
					else if (Token.X == j && Token.Y == i)
					{
						Console.Write("O");
					}
					else if (Goal.X == j && Goal.Y == i)
					{
						Console.Write("G");
					}
					else
					{
						Console.Write(" ");
					}
				}
				Console.WriteLine("");
			}
		}

		public bool IsEmptySpace(int x, int y)
		{
			return x < 0 || x >= MapWidth || y < 0 || y >= MapHeight || _map[y, x] == 0; 
		}

		public bool PlayerReachedGoal() { return Player.IsSamePosition(Goal); }

		#region IDeepCloneable<MapEntity>

		public Board DeepClone()
		{
			return new Board(_map, Player.DeepClone(), Token.DeepClone(),
												Goal.DeepClone());
		}

		object IDeepCloneable.DeepClone()
		{
			return DeepClone();
		}

		#endregion
	}
}
