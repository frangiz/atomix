﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix
{
	public class Game
	{
		private Board _board;
		private CommandManager _cmdManager;
		private EventManager _eventManager;
		private bool _done;
		private IInputHandler _inputHandler;
		private ConsoleLog _log;
		private int _nbrOfMovesMade = 0;

		public Game(Board board, IInputHandler inputHandler, CommandManager cmdManager, ConsoleLog log)
		{
			_board = board;
			_inputHandler = inputHandler;
			_done = false;
			_cmdManager = cmdManager;
			_eventManager = new EventManager(_board);
			_log = log;
			_eventManager.PlayerFinished += OnPlayerFinished;
			_eventManager.PlayerDead += _inputHandler.OnPlayerDead;
			_eventManager.PlayerDead += OnPlayerDead;

			cmdManager.CommandExecuted += OnCommandExecuted;
		}

		public void Start()
		{
			_inputHandler.Quit += () => { _done = true; };

			while (!_done)
			{
				Console.WriteLine(string.Format("Moves left: {0}", (_board.MaxMoves - _nbrOfMovesMade)));
				Console.WriteLine("");
				_board.DrawMap();
				_log.WriteLines();
				var cmd = _inputHandler.GetNextCommand(_board);
				if (cmd != null)
				{
					Console.Clear();
					_cmdManager.ExecuteCommand(cmd);
				}
			}
		}

		private void OnPlayerFinished()
		{
			_board.DrawMap();
			Console.WriteLine("Player finished!");
			_log.WriteLines();
			Console.ReadLine();
			_done = true;
		}

		private void OnPlayerDead(PlayerDeadEventArgs args)
		{
			if (args.Handled) { return; }
			_board.DrawMap();
			Console.WriteLine("Player dead :(");
			_log.WriteLines();
			Console.ReadLine();
			_done = true;
		}

		private void OnCommandExecuted()
		{
			_nbrOfMovesMade++;
			if (_board.MaxMoves == _nbrOfMovesMade)
			{
				Console.WriteLine("Game over.");
				Console.ReadLine();
				_done = true;
			}
		}

	}
}
