﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix.Test
{
	internal class TestMapLoader
	{
		public static Board LoadMap(int level)
		{
			if (level == 1) { return LoadLevelOne(); }
			if (level == 2) { return LoadLevelTwo(); }
			return null;
		}

		private static Board LoadLevelOne()
		{
			var map = new int[,]
			{
				{ 0, 0, 0, 0, 1, 0, 0, 0 }, 
				{ 1, 0, 0, 0, 0, 0, 0, 1 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 0, 0, 0, 1, 0, 0 }
			};
			var player = new MapEntity("kalle") { X = 2, Y = 2 };
			var token = new MapEntity("token") { X = 0, Y = 0 };
			var goal = new MapEntity("goal") { X = 7, Y = 2 };
			return new Board(map, player, token, goal);
		}

		private static Board LoadLevelTwo()
		{
			var map = new int[,]
			{
				{ 0, 1, 0, 0 }, 
				{ 0, 0, 0, 0 },
				{ 1, 0, 0, 1 },
				{ 0, 1, 0, 0 },
				{ 0, 0, 0, 0 }
			};
			var player = new MapEntity("kalle") { X = 1, Y = 2 };
			var token = new MapEntity("token") { X = 0, Y = 0 };
			var goal = new MapEntity("goal") { X = 2, Y = 0 };
			return new Board(map, player, token, goal);
		}
	}
}
