﻿using Atomix.AI;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atomix.Test
{
	[TestFixture]
	public class AIPlayerTest
	{
		private AIPlayer _aiPlayer;

		[SetUp]
		public void Setup()
		{
			_aiPlayer = new AIPlayer(new Mock<ILog>().Object);
		}

		[Test]
		public void AllInitialMovesArePossible()
		{
			var board = TestMapLoader.LoadMap(1);
			_aiPlayer.GetNextCommand(board);

			Assert.AreEqual(4, _aiPlayer.RootNode.Children.Count);
		}

		[Test]
		public void RemoveDeadPathsRemovesSimpleLeaves()
		{
			var board = TestMapLoader.LoadMap(2);
			_aiPlayer.GetNextCommand(board);

			Assert.AreEqual(2, _aiPlayer.RootNode.Children.Count);
			_aiPlayer.RemoveDeadPaths();
			Assert.AreEqual(1, _aiPlayer.RootNode.Children.Count);
			Assert.AreEqual(true, _aiPlayer.RootNode.HasGoalState());
		}

	}
}
